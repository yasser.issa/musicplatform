**- About -**
<br/>

It is about music platform backend project *(BLD_Backend_phase)* using django.<br/>
It consists of ***9_task*** starting from just **creating models (Artists && Albums && Songs) and relationships between them**,<br/> **creating forms,templates and views**<br/> going through:-<br/> **DRF**,<br/> **Authentication**,<br/> **Authorization**,<br/> **testing using pytest**,<br/> **running asynchronous tasks using celery.**

<br/>

# 9_Tasks done, each task is a branch in the project

# each branch has its own README
